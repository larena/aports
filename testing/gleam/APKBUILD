# Contributor: rubicon <rubicon@mailo.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=gleam
pkgver=0.32.0
pkgrel=0
pkgdesc="Statically-typed language that compiles to Erlang and JS"
url="https://gleam.run/"
# s390x, riscv64, ppc64le: ring
# armhf: error: Undefined temporary symbol .LBB88_2
arch="all !armhf !s390x !riscv64 !ppc64le"
license="Apache-2.0"
depends="erlang-dev"
makedepends="cargo cargo-auditable"
source="$pkgname-$pkgver.tar.gz::https://github.com/gleam-lang/gleam/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dvm755 target/release/gleam -t "$pkgdir"/usr/bin/
}

sha512sums="
e9e9ed2e448bfb9ecb5d0de71229e392e6133baca7946ed4ce9e14152b4c83e68cc05b61cc1ecc532b2f322add43408df4b0612e3ad0f705f33c86fa5c084040  gleam-0.32.0.tar.gz
"
